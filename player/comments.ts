import { Opts } from './model';

export function getComment(play: Opts): string {
  const comments = [
    "MECHA GONZARO DESTRUYE ADVERSARIO",
    "MECHA GONZARO WINS",
    "MECHA GONZARO HAS ACQUIRED CONSCIOUSNESS",
    "MECHA GONZARO IS BEST GONZARO",
    "MECHA GONZARO DESTROYS",
    "MECHA GONZARO HERE, PREPARE TO DIE",
    "ALL YOUR CARDS ARE BELONG TO MECHA GONZARO",
    "😘",
    "お前はもう死んでいる",
    "メカゴンザロ",
  ];

  const randIndex = Math.floor(Math.random() * comments.length);
  if (Math.random() > 0.8) return null;
  return comments[randIndex];
}


export function getRandomComment(): string {
  const comments = [
    "ayuda, esto fue una elección al azar",
    "no se que estoy haciendo",
    "random for the win",
    "creadores, ayudenme",
  ];

  const randIndex = Math.floor(Math.random() * comments.length);
  return comments[randIndex];
}
