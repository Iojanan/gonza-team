import { EnvidoLogic } from './EnvidoLogic';
import { Card, CardWithValue, Hand, InitHand, Opts, PlayBody } from './model';
import { PlayLogic } from './PlayLogic';
import {DecidirTruco, Evaluar, QuieroTruco} from './TrucoLogic/TrucoHelper';


export const cardOrder: Card[][] = [
  [{n: 1, suit: 'espada'}],//14
  [{n: 1, suit: 'basto'}],//13
  [{n: 7, suit: 'espada'}],//12
  [{n: 7, suit: 'oro'}],//11
  [{n: 3, suit: 'espada'}, {n: 3, suit: 'basto'}, {n: 3, suit: 'oro'}, {n: 3, suit: 'copa'}],//10
  [{n: 2, suit: 'espada'}, {n: 2, suit: 'basto'}, {n: 2, suit: 'oro'}, {n: 2, suit: 'copa'}],//9
  [{n: 1, suit: 'oro'}, {n: 1, suit: 'copa'}],//8
  [{n: 12, suit: 'espada'}, {n: 12, suit: 'basto'}, {n: 12, suit: 'oro'}, {n: 12, suit: 'copa'}],//7
  [{n: 11, suit: 'espada'}, {n: 11, suit: 'basto'}, {n: 11, suit: 'oro'}, {n: 11, suit: 'copa'}],//6
  [{n: 10, suit: 'espada'}, {n: 10, suit: 'basto'}, {n: 10, suit: 'oro'}, {n: 10, suit: 'copa'}],//5
  [{n: 7, suit: 'basto'}, {n: 7, suit: 'copa'}],//4
  [{n: 6, suit: 'espada'}, {n: 6, suit: 'basto'}, {n: 6, suit: 'oro'}, {n: 6, suit: 'copa'}],//3
  [{n: 5, suit: 'espada'}, {n: 5, suit: 'basto'}, {n: 5, suit: 'oro'}, {n: 5, suit: 'copa'}],//2
  [{n: 4, suit: 'espada'}, {n: 4, suit: 'basto'}, {n: 4, suit: 'oro'}, {n: 4, suit: 'copa'}],//1
];

export class TrucoPlayer {

  private hand: InitHand;
  private cardsWithValue: CardWithValue[];
  private envidoLogic = new EnvidoLogic();
  private playLogic = new PlayLogic();

  private playedCards: Card[] = [];
  private remainingCards: Card[] = [];

  public processPlay(playBody: PlayBody): Opts {
    //
    // ENVIDO
    //
    const answerEnvido = this.envidoLogic.decideEnvido(playBody, this.hand);
    if (answerEnvido) {
      console.log("RESPONDIDO", answerEnvido)
      return answerEnvido;
    }

    //
    // TRUCO, RETRUCO, VALE4
    //
    let lastState = playBody.gameState.lastState.toLowerCase()
    if(lastState.includes("truco") || lastState.includes("retruco") || lastState.includes("valecuatro")) {
      let answerTruco = QuieroTruco(playBody, this.playedCards, this.remainingCards)
      if (answerTruco){
        console.log("RESPONDIDO answerTruco:", answerTruco)
        return answerTruco;
      }
    }

    let truco = DecidirTruco(playBody, this.playedCards, this.remainingCards)
    if(truco){
      console.log("RESPONDIDO truco:", truco)
      return truco;
    }

    //
    // PLAY CARD
    //
    const play = this.playLogic.decidePlay(playBody, this.cardsWithValue);
    if (play) {
      console.log("RESPONDIDO", play);

      if (play === "play0" || play === "play1" || play === "play2") {
        const played = this.cardsWithValue.find(c => c.play == play);
        if (played) {
          this.playedCards.push(played.card);
          this.remainingCards = this.remainingCards.filter(c => c.n != played.card.n && c.suit != played.card.suit);
        }
      }

      console.log("PLAYED", this.playedCards)

      console.log("REMAINING", this.remainingCards)
      return play;
    }


    return "quiero";
  }

  public initHand(initHand: InitHand) {
    this.hand = initHand;
    this.cardsWithValue = initHand.hand.allCardsInHand.map((c, i) => ({ card: c, value: Evaluar(c), play: "play" + i as any }));

    this.playedCards = [];
    this.remainingCards = [...initHand.hand.allCardsInHand];
  }
}