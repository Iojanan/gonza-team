import { EnvidoLogic } from '../EnvidoLogic';

describe("Envido test", () => {

  const el = new EnvidoLogic();

  test("It should run lowest card ", () => {
    const res = el.decideEnvido({
      gameState: {
        myScore: 3,
        oponentScore: 1,
        lastState: "playCard",
      },
      opts: ["envido"]
    } as any,
    {
      hand: {
        allCardsInHand: [{n: 2, suit: "copa"}, {n: 10, suit: "copa"}, {n: 7, suit: "oro"}],
      }
    } as any);

    expect(res).toBe(null);
  });


  test("It SHOULD NO QUIERO ", () => {
    const res = el.decideEnvido({
      gameState: {
        myScore: 3,
        oponentScore: 1,
        lastState: "faltaEnvido",
      },
      opts: ["quiero", "noQuiero"]
    } as any,
    {
      hand: {
        allCardsInHand: [{n: 2, suit: "copa"}, {n: 10, suit: "espada"}, {n: 7, suit: "oro"}],
      }
    } as any);

    expect(res).toBe("noQuiero");
  });

});