import {Evaluar, Puntuar, QuieroTruco} from '../TrucoLogic/TrucoHelper';
import {Card, Envido, LastState, PlayBody} from "../model";


describe("Test Tomas 1", () => {
  describe("QuieroTruco", () => {
    test("It should return card value", done => {

      const value = QuieroTruco({
        name: "test 1",
        opts: ["reTruco", "noQuiero"],
        gameState: {
          myScore: 0,
          oponentScore: 0,
          oponentPlayedCards: [],
          lastState: "truco",
          envido: null
        }
      }, [
          {n: 7, suit: 'oro'}, {n: 12, suit: 'espada'}, {n: 11, suit: 'basto'}
          ],
          []
      );

      expect(value).toBe("noQuiero");
      done();
    });
  });

  describe("QuieroTruco", () => {
    test("It should return card value", done => {

      const value = QuieroTruco({
        name: "test 1",
        opts: ["reTruco", "noQuiero"],
        gameState: {
          myScore: 0,
          oponentScore: 0,
          oponentPlayedCards: [],
          lastState: "truco",
          envido: null
        }
      },  [

          ],
          [{n: 7, suit: 'oro'}, {n: 1, suit: 'espada'}, {n: 11, suit: 'basto'}],
          );

      expect(value).toBe("reTruco");
      done();
    });
  });

});