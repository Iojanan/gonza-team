import { Evaluar, Puntuar } from '../TrucoLogic/TrucoHelper';


describe("Test Mecha Gonzaro", () => {
  test("It should run", done => {
    done()
  });
  describe("Test card evaluation", () => {
    test("It should return card value", done => {
      const value = Evaluar({n: 3, suit: 'espada'});
      expect(value).toBe(10);
      done();
    });
  });
});