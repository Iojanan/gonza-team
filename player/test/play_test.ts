import { Card } from '../model';
import { PlayLogic } from '../PlayLogic';
import { Evaluar } from '../TrucoLogic/TrucoHelper';

describe("Test play card", () => {

  const play = new PlayLogic();

  const initHand = (cards: Card[]) => cards.map((c, i) => ({ card: c, value: Evaluar(c), play: "play" + i as any }))

  test("It should run lowest card ", () => {
    const res = play.decidePlay({
      opts: ["play0", "play1", "play2"],
      gameState: {
        oponentPlayedCards: []
      } as any
    } as any,
    initHand([{n: 7, suit: "espada"}, {n: 2, suit: "copa"}, {n: 5, suit: "basto"}])
    );
    expect(res).toBe("play2");
  });

  test("It should run middle card ", () => {
    const res = play.decidePlay({
      opts: ["play0", "play1", "play2"],
      gameState: {
        oponentPlayedCards: []
      } as any
    } as any,
    initHand([{n: 7, suit: "espada"}, {n: 2, suit: "copa"}, {n: 1, suit: "basto"}])
    );
    expect(res).toBe("play0");
  });

  test("It should run lowest card that wins ", () => {
    const res = play.decidePlay({
      opts: ["play0", "play1", "play2"],
      gameState: {
        oponentPlayedCards: [{n: 3, suit: "oro"}]
      } as any
    } as any,
    initHand([{n: 7, suit: "espada"}, {n: 2, suit: "copa"}, {n: 1, suit: "basto"}])
    );
    expect(res).toBe("play0");
  });

  test("It should run lowest card ", () => {
    const res = play.decidePlay({
      opts: ["play0", "play1", "play2"],
      gameState: {
        oponentPlayedCards: [{n: 1, suit: "espada"}]
      } as any
    } as any,
    initHand([{n: 7, suit: "espada"}, {n: 2, suit: "copa"}, {n: 1, suit: "basto"}])
    );
    expect(res).toBe("play1");
  });
});