
module.exports = {
  apps : [{
    name: "truco-app",
    script: "./build/server.js",
    instances: "max",
    exec_mode: "cluster",
    env: {
      NODE_ENV: "prod",
    }
  }]
}