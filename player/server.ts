import express from 'express'
import { getComment, getRandomComment } from './comments';
import { Hand, InitHand, PlayBody } from './model';
import { TrucoPlayer } from './TrucoPlayer';

const app = express()
const port = Number(process.env.PORT) | 4000


const player = new TrucoPlayer();

app.use(express.json());
app.get('/', function(req,res){
    res.send({ status: 200, message: "Mecha Gonzaro booted up and ready to smash!" });
});

app.get('/test', function(req,res){
    res.send({ status: 200, message: "Mecha Gonzaro booted up and ready to smash!" });
});

app.post('/processPlay', async function (req,res) {
    const body: PlayBody = req.body;
    try {
        const play = player.processPlay(body);
        if (!req.body.opts.includes(play)) {
            console.log("OPTS", req.body.opts)
            console.log("JUGADO", play)
            // RANDOM OPTION #YOLO
            const option = Math.floor(Math.random() * (req.body.opts.length-1)) ;
            res.send({status: 200, play: req.body.opts.filter((o: string) => o !== 'mazo')[option], comment: getRandomComment() });
            return;
        } else {
            res.send({ status: 200, play, comment: getComment(play) });
            return;
        }
    } catch (error) {
        console.log("ERROR", error)
        // RANDOM OPTION #YOLO
        const option = Math.floor(Math.random() * (req.body.opts.length-1)) ;
        res.send({status: 200, play: req.body.opts.filter((o: string) => o !== 'mazo')[option], comment: getRandomComment() });
        return;
    }
});

app.post('/initHand', async function (req, res) {
    const body: InitHand = {...req.body, hand: JSON.parse(req.body.hand)};
    player.initHand(body);
    res.send({status: 200, message: "Mecha Gonzaro booted up and ready to smash!"});
});

app.listen(port, () => {
    console.log(`メカゴンザロ is listening on port ${port}, お前はもう死んでいる!`);
});
