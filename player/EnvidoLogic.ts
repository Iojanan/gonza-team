import { Hand, InitHand, Opts, PlayBody } from './model';

export class EnvidoLogic {

  public decideEnvido(playBody: PlayBody, hand: InitHand): Opts | null {
    const { myScore, oponentScore, lastState } = playBody.gameState;
    const envidoNumber = this.countEnvido(hand.hand);
    let answer: Opts | null = null;

    const confidence = this.confidenceCalculator(myScore, oponentScore);

    // nos cantaron a nosotros?
    if (lastState.toLowerCase().includes("envido")) {
      switch(lastState) {
        case "envido":
          answer = this.envidoScenario(envidoNumber, confidence, myScore);
          break;
        case 'envidoEnvido':
          answer = this.envidoEnvidoScenario(envidoNumber, confidence, myScore);
          break;
        case 'realEnvido':
          answer = this.realEnvidoScenario(envidoNumber, confidence, myScore);
          break;
        case "envidoEnvidoRealEnvido":
          answer = this.envidoEnvidoRealEnvido(envidoNumber, confidence, myScore);
          break;

        case "faltaEnvido":
        case "envidoEnvidoFaltaEnvido":
        case "envidoRealEnvidoFaltaEnvido":
        case "envidoEnvidoRealEnvidoFaltaEnvido":
        case "envidoRealEnvidoFaltaEnvido":
        case "realEnvidoFaltaEnvido":
          answer = this.faltaEnvidoScenario(envidoNumber, confidence, myScore, oponentScore);
          break;
      }
    } else if (playBody.opts.includes("envido") || playBody.opts.includes("Envido" as any)) {
      // nosotros podemos cantar?

      if (envidoNumber > 24 - confidence ) {
        answer = "envido";
      }

      if (envidoNumber > 27 - confidence) {
        answer = "realEnvido";
      }

      if (envidoNumber > 30 - confidence) {
        answer = "faltaEnvido";
      }

      if (oponentScore == 29) {
        answer = "envido";
      }
    }

    return answer;
  }

  public envidoScenario(envidoNumber: number, confidence: number, myScore: number): Opts {
    if (myScore >= 29) {
      return "faltaEnvido";
    }

    if (envidoNumber > 27 - confidence) {
      return "realEnvido";
    }
    if (envidoNumber > 30 - confidence) {
      return "realEnvido";
    }
    if (envidoNumber > 32 - confidence) {
      return "faltaEnvido";
    }

    if (envidoNumber > 24 - confidence) {
      return "quiero";
    }

    return "noQuiero";
  }

  public envidoEnvidoScenario(envidoNumber: number, confidence: number, myScore: number): Opts {
    if (myScore >= 29) {
      return "faltaEnvido";
    }

    if (envidoNumber > 30 - confidence) {
      return "realEnvido";
    }
    if (envidoNumber > 32 - confidence) {
      return "faltaEnvido";
    }

    if (envidoNumber > 27 - confidence) {
      return "quiero";
    }

    return "noQuiero";
  }

  public realEnvidoScenario(envidoNumber: number, confidence: number, myScore: number): Opts {
    if (myScore >= 29) {
      return "faltaEnvido";
    }

    if (envidoNumber > 32 - confidence) {
      return "faltaEnvido";
    }

    if (envidoNumber > 28 - confidence) {
      return "quiero";
    }

    return "noQuiero";
  }

  public envidoEnvidoRealEnvido(envidoNumber: number, confidence: number, myScore: number): Opts {
    if (myScore >= 29) {
      return "faltaEnvido";
    }

    if (envidoNumber > 32 - confidence) {
      return "faltaEnvido";
    }

    if (envidoNumber > 29 - confidence) {
      return "quiero";
    }

    return "noQuiero";
  }

  public faltaEnvidoScenario(envidoNumber: number, confidence: number, myScore: number, oponentScore: number): Opts {
    if (oponentScore == 29) {
      return "quiero";
    }

    if (envidoNumber > 30 - confidence) {
      return "quiero";
    }
    return "noQuiero";
  }

  // Higher the number => Higher Confidence
  // Negative number => low Confidence
  public confidenceCalculator(myScore: number, oponentScore: number): number {
    return Math.floor((myScore - oponentScore) / 10);
  }

  public countEnvido(hand: Hand): number {
    const suits: {
        [key: string]: number[];
      } = {
        'espada': [],
        'basto': [],
        'oro': [],
        'copa': [],
    }
    let highestValueCard = 0;
    let envidoSuit = ''
    hand.allCardsInHand.forEach(c => {
        const n = c.n < 10 ? c.n : 0
        if (suits[c.suit].length === 0) {
            suits[c.suit].push(n);
        } else {
            if (n >= suits[c.suit][0]) {
                suits[c.suit].unshift(n);
            } else if (n <= suits[c.suit][suits[c.suit].length - 1]) {
                suits[c.suit].push(n);
            } else {
                suits[c.suit].splice(1, 0, n);
            }
        }
        if (suits[c.suit].length >= 2) {
            envidoSuit = c.suit;
        }
        if (c.n < 10 && c.n > highestValueCard) {
            highestValueCard = c.n;
        }
    });

    if (envidoSuit === '') {
        return highestValueCard;
    }

    return suits[envidoSuit][0] + suits[envidoSuit][1] + 20;
  }
}

/*
const hand = [{n: 6, suit: "copa"}, {n: 4, suit: "oro"},{n: 3, suit: "basto"}]
const tantos = new EnvidoLogic().countEnvido({allCardsInHand: hand} as any)
console.log("TANTOS", tantos);
*/