import { CardWithValue, InitHand, Opts, PlayBody } from './model';
import * as _ from "lodash";
import { Evaluar } from './TrucoLogic/TrucoHelper';

export class PlayLogic {

  public decidePlay(playBody: PlayBody, cardsWithValue: CardWithValue[]): Opts | null {
    if (playBody.opts.includes("play0") || playBody.opts.includes("play1") || playBody.opts.includes("play2")) {
      // SOMOS MANO Y TENEMOS QUE JUGAR LA PRIMERA
      if (playBody.gameState.oponentPlayedCards.length === 0) {
        const lowest = _.min(cardsWithValue.map(c => c.value))
        if (lowest <= 5) {
          return cardsWithValue.find(c => c.value == lowest).play;
        }

        const middle = _.sortBy(cardsWithValue, "value")[1];
        return middle.play;

      } else {
        // EL CONTRINCANTE YA JUGO ALGUNA CARTA
        const lastCardPlayed = playBody.gameState.oponentPlayedCards[playBody.gameState.oponentPlayedCards.length - 1];
        const otherCardValue = Evaluar(lastCardPlayed);

        // JUGAMOS LA MÁS CHICA QUE LE GANA
        const winnerCards = cardsWithValue.filter(c => c.value > otherCardValue);
        if (winnerCards.length > 0) {
          return _.minBy(winnerCards, "value").play;
        }

        // JUGAMOS LA QUE EMPATA
        const pardaCards = cardsWithValue.filter(c => c.value === otherCardValue);
        if (pardaCards.length > 0) {
          return pardaCards[0].play;
        }

        // JUGAMOS LA MÁS CHICA SI NO LE GANAMOS
        return _.minBy(cardsWithValue, "value").play;
      }
    }
  }
}