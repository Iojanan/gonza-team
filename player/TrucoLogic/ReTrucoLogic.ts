import {Card} from "../model";
import * as TrucoLogic from './TrucoHelper';
import {secureHeapUsed} from "crypto";

const threshold = 9

export function ReTruco(cartas: Card[]){
    let ronda = (3 - cartas.length) + 1
    if(ronda == 1)
        return ReTruco_ronda1(cartas)

    if(ronda == 2)
        return ReTruco_ronda2(cartas)

    return ReTruco_ronda3(cartas)

}

function ReTruco_ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 3)
        return 0.9
    if(suma == 2)
        return 0.7
    return 0
}

function ReTruco_ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.7
    return 0
}

function ReTruco_ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold + 3)
    if(suma == 1)
        return 1
    return 0
}



export function QuieroReTruco(cartas: Card[]){
    let ronda = (3 - cartas.length) + 1

    if(ronda == 1)
        return QuieroReTruco_Ronda1(cartas)
    if(ronda == 2)
        return QuieroReTruco_Ronda2(cartas)

    return QuieroReTruco_Ronda3(cartas)
}

function QuieroReTruco_Ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 3)
        return 1

    if(suma == 2)
        return 0.7

    if(suma == 1)
        return 0.3

    return 0//me voy a al maso, no tengo nada jaja
}
function QuieroReTruco_Ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.8
    return 0
}
function QuieroReTruco_Ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold + 3)

    if(suma)
        return 1
    return 0
}