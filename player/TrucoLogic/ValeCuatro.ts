import {Card} from "../model";
import * as TrucoLogic from './TrucoHelper';
import {secureHeapUsed} from "crypto";

const threshold = 11

export function ValeCuatro(cartas: Card[]){
    let ronda = (3 - cartas.length) + 1
    if(ronda == 1)
        return ValeCuatro_ronda1(cartas)

    if(ronda == 2)
        return ValeCuatro_ronda2(cartas)

    return ValeCuatro_ronda3(cartas)

}

function ValeCuatro_ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 3)
        return 0.9
    if(suma == 2)
        return 0.5
    return 0
}

function ValeCuatro_ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.7
    return 0
}

function ValeCuatro_ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold + 3)
    if(suma == 1)
        return 1
    return 0
}



export function QuieroValeCuatro(cartas: Card[]){
    let ronda = (3 - cartas.length) + 1

    if(ronda == 1)
        return QuieroValeCuatro_Ronda1(cartas)
    if(ronda == 2)
        return QuieroValeCuatro_Ronda2(cartas)

    return QuieroValeCuatro_Ronda3(cartas)
}

function QuieroValeCuatro_Ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 3)
        return 1

    if(suma == 2)
        return 0.7

    if(suma == 1)
        return 0.3

    return 0//me voy a al maso, no tengo nada jaja
}
function QuieroValeCuatro_Ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.8
    return 0
}
function QuieroValeCuatro_Ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold + 2)

    if(suma)
        return 1
    return 0
}