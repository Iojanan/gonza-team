import {Card, InitHand, Opts, PlayBody} from "../model";
import * as _ from "lodash";
import * as TrucoPlayer from "../TrucoPlayer";
import * as TrucoLogic from "./TrucoLogic";
import * as ReTrucoLogic from "./ReTrucoLogic";
import * as ValeCuatro from "./ValeCuatro";
import {head} from "lodash";
import {getRandomComment} from "../comments";

export function Evaluar(carta: Card){

    let index = 0
    for (let cartas of TrucoPlayer.cardOrder) {
        for (let cartaItem of cartas){
            //console.log(JSON.stringify(carta) + "  " + JSON.stringify(cartaItem))
            if(_.isEqual(carta, cartaItem)) {
                //console.log("IS EQUAL!, index:" + index + "  value: " + (TrucoPlayer.cardOrder.length - index))
                return TrucoPlayer.cardOrder.length - index
            }
        }
        index++
    }

    console.error("ERROR ERROR no se encontro la carta" + JSON.stringify(carta))
    return 0
}

export function Puntuar(cartas: Card[], threshold: number){
    let puntos = 0
    for(let carta of cartas){
        if(Evaluar(carta) > threshold)
            puntos++
    }
    console.log("Puntuar?" + puntos + " " + JSON.stringify(cartas))
    return puntos
}

//si aun no la jugamos no la ganamos...
export function Ganamos(playBody: PlayBody, playedCards: Card[], ronda: number){
    try{
        if(playBody.gameState.oponentPlayedCards.length <= ronda)
            return false

        let oponent_card = playBody.gameState.oponentPlayedCards[ronda - 1]
        let my_card = playedCards[ronda - 1]
        return Evaluar(oponent_card) < Evaluar(my_card)
    } catch (error) {
        console.log("ERROR de ganamos.. nada grave.. ", error)
        return false;
    }
}



//cuando me cantan truco, retruco o vale cuatro
export function QuieroTruco(playBody: PlayBody, playedCards: Card[], myCards: Card[]): Opts | null {
    const { myScore, oponentScore, lastState } = playBody.gameState;
    let value = 0

    if(playBody.gameState.oponentPlayedCards.length == 3) {
        //si es la ultima y ya tiro! y tengo algo bueno la subo!, si me subieron re truco SE LA SIGO SUBIENDO!!
        if (Evaluar(playBody.gameState.oponentPlayedCards[2]) < Evaluar(myCards[0])){
            return DecidirTruco(playBody, playedCards, myCards, true)
        }
    }

    //me cantaron truco?
    if(lastState.toLowerCase().includes("truco")){
        console.log("Truco?" + TrucoLogic.QuieroTruco(myCards))
        value = TrucoLogic.QuieroTruco(myCards)
    }else if(lastState.toLowerCase().includes("retruco")){
        value = ReTrucoLogic.QuieroReTruco(myCards)
    }else if(lastState.toLowerCase().includes("valecuatro")){
        value = ValeCuatro.QuieroValeCuatro(myCards)
    }

    //si estan por ganar con, se lo subo!
    if(playBody.gameState.oponentScore >= 29){
        return DecidirTruco(playBody, playedCards, myCards, true)
    }

    if(value < 0.2)
        return "noQuiero"

    //si quiero turco, lo quiero subir diciendo re truco?
    let value_subir = DecidirTruco(playBody, playedCards, myCards)
    console.log("subir? decidirtruco?: " + value_subir)

    if(value_subir)
        return value_subir

    return "quiero"
}

//pregunto si quiero subir, a truco o retruco o a vale cuatro
export function DecidirTruco(playBody: PlayBody, playedCards: Card[], myCards: Card[], subir: boolean = false): Opts | null {

    let agresion = 0
    if(!subir) {
        //si estan por ganar con 29, se lo subo!
        if (playBody.gameState.oponentScore >= 29) {
            return DecidirTruco(playBody, playedCards, myCards, true)
        }

        if (playBody.gameState.oponentPlayedCards.length == 3) {
            //si es la ultima y ya tiro! y tengo algo bueno la subo!
            if (Evaluar(playBody.gameState.oponentPlayedCards[2]) < Evaluar(myCards[0])) {
                return DecidirTruco(playBody, playedCards, myCards, true)
            } else {
                //si es la ultima y no tengo nada, no la subas!
                return null;
            }
        }

        //si ganamos una ronda podemos ser mas agresivos!
        if (Ganamos(playBody, playedCards, 1) || Ganamos(playBody, playedCards, 2)) {
            agresion = 0.2
        }
    }

    if(playBody.opts.includes("truco")){
        if(subir || TrucoLogic.Truco(myCards, playBody) > 0.4 - agresion)
            return "truco"
    }else if(playBody.opts.includes("reTruco")){
        console.log("lo subo a re truco?" + ReTrucoLogic.ReTruco(myCards))
        if(subir || ReTrucoLogic.ReTruco(myCards) > 0.6 - agresion)
            return "reTruco"
    }else if(playBody.opts.includes("valeCuatro")){
        if(subir || ValeCuatro.ValeCuatro(myCards) > 0.6 - agresion)
            return "valeCuatro"
    }

    return null
}