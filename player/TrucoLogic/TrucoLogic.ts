import {Card, Hand, PlayBody} from "../model";
import * as _ from "lodash";
import * as TrucoLogic from './TrucoHelper';
import {TrucoPlayer} from "../TrucoPlayer";

let threshold = 7

//en cartas, tienen que ser las cartas con las que puedo juegar, en la ronda 1, espero 3 cartas. en la ronda 3 una carta
export function Truco(cartas: Card[], playBody: PlayBody){
    let ronda = (3 - cartas.length) + 1

    //para ganar o ganar..
    if(playBody.gameState.oponentScore >= 29)
        return 1

    if(ronda == 1)
        return Truco_Ronda1(cartas)
    if(ronda == 2)
        return Truco_Ronda2(cartas)
    return Truco_Ronda3(cartas)
}

function Truco_Ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 0)
        return 0 //no tengo nada..

    if(suma == 1)
        return 0.3

    if(suma == 2)
        return 0.8

    if(suma == 3)
        return 0.3
    return 0
}

function Truco_Ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.5

    return 0
}

function Truco_Ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma > 7)
        return 0.8
    return 0
}


export function QuieroTruco(cartas: Card[]){
    let ronda = (3 - cartas.length) + 1

    if(ronda == 1)
        return QuieroTruco_Ronda1(cartas)
    if(ronda == 2)
        return QuieroTruco_Ronda2(cartas)

    return QuieroTruco_Ronda3(cartas)
}

function QuieroTruco_Ronda1(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 3)
        return 1

    if(suma == 2)
        return 0.7

    if(suma == 1)
        return 0.3

    return 0//me voy a al maso, no tengo nada jaja
}
function QuieroTruco_Ronda2(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma == 2)
        return 0.8
    return 0
}
function QuieroTruco_Ronda3(cartas: Card[]){
    let suma = TrucoLogic.Puntuar(cartas, threshold)

    if(suma)
        return 1
    return 0
}

/*

console.log(Truco([{n: 7, suit: 'oro'}, {n: 12, suit: 'espada'}, {n: 11, suit: 'basto'}]))
console.log(Truco([{n: 7, suit: 'oro'}, {n: 11, suit: 'basto'}]))
console.log(Truco([{n: 11, suit: 'basto'}]))

console.log(QuieroTruco([{n: 7, suit: 'oro'}, {n: 12, suit: 'espada'}, {n: 11, suit: 'basto'}]))
console.log(QuieroTruco([{n: 7, suit: 'oro'}, {n: 11, suit: 'basto'}]))
console.log(QuieroTruco([{n: 11, suit: 'basto'}]))*/