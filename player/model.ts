export type InitHand = {
  hand: Hand;
  startedPlaying: boolean;
}


export type Hand = {
  allCardsInHand: Card[]
  playedCards: number[]
  remainingCards: number[]
}

export type Card = {
  n: number
  suit: Suit
}

export type Suit = 'espada' | 'basto' | 'oro' | 'copa'

export type EnvidoRound = {
  currentPlayerPoints: number
  oponentPoints?: number
}

export type Envido = {
  status?: string,
  handPlayerEnvidoPoints?: number,
  secondPlayerEnvidoPoints?: number
}

export type GameStateForPlayer = {
  myScore: number,
  oponentScore: number,
  oponentPlayedCards: Card[],
  lastState: string,
  envido?: Envido
};

export type CommandResponse = {
  play: string;
  comment?: string;
}


export type Opts = 'mazo' |  'envido' |  'realEnvido' | 'faltaEnvido' |  'play0' | 'play1' | 'play2' | 'truco' | 'reTruco' | "valeCuatro" | "quiero" | "noQuiero";

export type LastState =  "playCard" | "truco" | "reTruco" | "valeCuatro" | "envido" | 'envidoEnvido' | 'realEnvido' | "faltaEnvido" | "envidoEnvidoRealEnvido" | "envidoEnvidoFaltaEnvido" | "envidoRealEnvidoFaltaEnvido" | "envidoEnvidoRealEnvidoFaltaEnvido" | "envidoRealEnvidoFaltaEnvido" | "realEnvidoFaltaEnvido";

export type PlayBody = {
    name: string, 					// the name you picked
    opts: Opts[], 					// available options listed below, must pick one
    gameState: {
      myScore: number,
      oponentScore: number,
      oponentPlayedCards: Card[],
      lastState: LastState,
      envido?: Envido
    }
};

export type CardWithValue = { card: Card, play: "play0" | "play1" | "play2", value: number }